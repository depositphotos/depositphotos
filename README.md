# Depositphotos

[![build status](https://gitlab.com/depositphotos/depositphotos/badges/master/build.svg)](https://gitlab.com/depositphotos/depositphotos/commits/master)
[![coverage report](https://gitlab.com/depositphotos/depositphotos/badges/master/coverage.svg)](https://gitlab.com/depositphotos/depositphotos/commits/master)

## Install

    $ npm install depositphotos

## Usage

## Tests

    $ npm install
    $ npm test

## Credits

  - [Oleksii Volkov](https://gitlab.com/alexey-wild)

## License

[The MIT License](http://opensource.org/licenses/MIT)

Copyright (c) 1988-2017 Oleksii Volkov <[https://ovolkov.pro/](https://ovolkov.pro/)>
