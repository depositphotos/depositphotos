'use strict';

const querystring = require('querystring');
const fetch = require('node-fetch');
const Image = require('./image').Image;

const BASE_URL = 'https://api.depositphotos.com';

class Depositphotos {
  constructor(opts) {
    if (!opts.apiKey) {
      throw Error('Depositphotos API needs apiKey')
    }
    this.apiKey = opts.apiKey;

    this.image = new Image(this);
  }

  apiCall(command, params = {}) {
    let query = params;
    query['dp_affiliate_id'] = 15657;
    query['dp_apikey'] = this.apiKey;
    query['dp_command'] = command;

    return fetch(`${BASE_URL}/?${querystring.stringify(query)}`)
  }
}

module.exports = Depositphotos;
