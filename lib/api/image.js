'use strict';

class Image {
  constructor(api) {
    this.api = api;
  }

  /**
   * Required at least one of 'dp_search_query', 'dp_search_categories', 'dp_search_user', 'dp_search_username' parameters.
   * Example 1: http://api.depositphotos.com?dp_apikey=dp_command=search&dp_search_categories=12+36+47
   * Example 2: http://api.depositphotos.com?dp_apikey=dp_command=search&dp_search_query=pretty+woman&dp_search_sort=1&dp_search_nudity=1
   *
   * param string $dp_apikey Client API key
   * param string $dp_command Command name 'search'
   * param string $dp_domain Optional. Default is "depositphotos.com". Domain name of one of CDN servers available (e.g. imgcontent.net)
   * param string $dp_search_query A phrase in English, e.g. "woman hands" OR item id which we are looking for
   * param int $dp_search_sort Optional. Default is 4. Definition of the sort type (number from 1 to 6, each number equals sort type).
   * In fact, 2nd an 3rd search type are equal 4th now ("best_sales"). So you should use 1, 4, 5 and 6 sort type.
   *
   * 1 is for 'best_match'
   * 4 is for 'best_sales'
   * 5 is for 'newest'
   * 6 is for 'undiscovered'
   *
   * param int $dp_search_limit Optional. Default is 100. A limit to the number of returned search results
   * param string $dp_search_offset Optional. Default is 0. An offset for the first result to return, after sorting the found set.
   * param string $dp_search_categories Optional. Space-delimited list of category identifiers to search for (e.g. '34 35 23')
   * param int $dp_search_color Optional. Search by the dominant color of the image. List of colors:
   * 0 - any
   * 1 - blue, #00007c
   * 2 - blue, #0005fd
   * 3 - blue, #01ffff
   * 4 - green, #027f00
   * 5 - green, #04fe00
   * 6 - yellow, #ffff00
   * 7 - orange, #f9be00
   * 8 - orange, #fecd9b
   * 9 - red, #fe0000
   * 10 - red, #7e0004
   * 11 - brown, #653201
   * 12 - violet, #ff01ff
   * 13 - violet, #810081
   * 14 - grey, #bfbfbf
   * 15 - grey, #7a7a7a
   * 16 - black, #000000
   * 17 - white, #ffffff
   *
   * param bool $dp_search_nudity Optional. Default is 0. If false, any search resuts containing nudity will be hidden, otherwise all images.
   * param int $dp_search_user Optional. Search by the author, using an author identifier (e.g. 1000942)
   * param string $dp_search_username Optional. Search by username of DepositPhotos user
   * param string $dp_search_date1 Deprecated. Search starting a speified image publishing date (e.g. '2010-01-01')
   * param string $dp_search_date2 Deprecated. Search up to the specified image publishing date (e.g. '2010-05-01')
   * param string $dp_search_orientation Optional. Search by the image orientation. [ 'horizontal' | 'vertical' | 'square' ]
   * param string $dp_search_imagesize Optional. Search by the image size. Sets minimum image size. [ 's' | 'm' | 'l' | 'xl' ]
   * param string $dp_exclude_keyword Optional. Comma-separated words to exclude.
   * param bool $dp_search_photo Optional. Default is true. If true, the search results will include JPEG images. If false - exclude.
   * param bool $dp_search_vector Optional. Default is true . If true, the search results will include vector images. If false - exclude.
   * param bool $dp_search_video Optional. Default is false. If true, the search results will include videos. If false - exclude.
   * param bool $dp_search_editorial Optional. If true, the searh results will include only editorial items. If false - exclude. If not passed, this filter won't be applied.
   * param string $dp_tracking_url Optional. Affilate tracking link.
   * param bool $dp_full_info Optional. Default is false. Return full info about items.
   * param string $dp_watermark Optional. Watermark to use: depositphotos | neutral. Default is neutral.
   * param string $dp_translate_items Optional. Default is false. If true - title, description and categories will be translated (if translation exists) to language $dp_lang
   * param string $dp_lang Optional. Default is 'en'. Language for translation (de, ru, fr, sp, zh (for chinese) etc.)
   * param bool $dp_search_correction Optional. Default is '1'. Enable auto correction of search phrase.
   * param int $dp_search_height Optional. Minimum image height with units in dp_search_dimension_units.
   * param int $dp_search_width Optional. Minimum image width with units in dp_search_dimension_units.
   * param int $dp_search_max_height Optional. Maximum image height with units in dp_search_dimension_units.
   * param int $dp_search_max_width Optional. Maximum image width with units in dp_search_dimension_units.
   * param string $dp_search_dimension_units Optional. Default = 'px'. Units for min and max image search size. Px to inc treats as 300 DPI. [ 'px' | 'inch' | 'cm' ]
   * param string $dp_image_url Optional. Search by image.
   * param string $dp_search_gender People gender [ 'male' | 'female' | 'both' ]
   * param bool $dp_search_people_only Only people must be present
   * param str|int $dp_search_age People age [ 'infant' | 'child' | 'teenager' | '20' | '30' | '40' | '50' | '60' | '70' ]
   * param string $dp_search_race People race [ 'asian' | 'brazilian' | 'black' | 'caucasian' | 'hispanic' | 'middle' | 'multi' | 'native' | 'other' ]
   * param int $dp_search_quantity People quantity in the image. Means 'any' if greater than 3. [ 1 | 2 | 3 ]
   * param string $dp_item_permission Optional. Search some special type of files. [ "regular" - All files (default) | "enterprise" - Curated Collection (For ES users only) ]
   *
   * return array
   * {
 *   timestamp: "2013-03-12 11:12:54",   // The current date and time in YYYY-MM-DD HH:MI:SS format
 *   version: "1.3",                     // An API version
 *   result:[                            // An array of search results
 *   {
 *       id: 3366293,                                                // Item id
 *       thumbnail: "http://static4.depo...office-desk.jpg",         // A link to the image small preview
 *       medium_thumbnail: " http://static...positphotos_1786993-Cat.jpg
 *       url: "http://s...office-desk.jpg",                          // A link to the image small preview
 *       url2: "http://s...api_thumb_450.jpg",                       // A link to the medium image preview with a neutral watermark
 *       url_big: "http://s...ice-desk.jpg",                         // A link to the big image preview with Depositphotos watermark
 *       url_max_qa: "http://s...ice-desk.jpg",                      // A link to the big image preview with Depositphotos watermark
 *       itemurl: "http://depositphotos.com/...office-desk.html",    // A link to the image preview on www.depositphotos.com
 *       mp4: "http://s...../depositphotos_1232343-item-title.mp4    // If item type is video
 *       webm: "http://s...../depositphotos_1232343-item-title.webm  // If item type is video
 *       published: "Jan.25, 2010 12:31:33",
 *       updated: "Jan.24, 2010 02:12:29",
 *       itype: "image",
 *       iseditorial: false,                                         // Defines if the item can be sold for editorial use only
 *       title: "Pretty Caucasian business woman at office desk",    // Item title
 *       description: "Pretty Caucasian ... in the back",            // Item description
 *       userid: 1011061,                                            // Image author id
 *       username: "Alexxx"                                          // Authors username
 *       avatar: "",                                                 // Author avatar
 *       status: "active"                                            // Item status. Sometimes search can return deleted item. [ 'active' | 'deleted' ]
 *       itype: "image",                                             // Item type, takes on the values of image/vector/video
 *       width: 6048,                                                // Original image width
 *       height: 4032,                                               // Original image height
 *       mp: 24.385536,                                              // Original image resolution
 *       exclusive: "no",                                            // Defines if the item is exlusive
 *       editorial: "no",                                            // Defines if the item can be sold for editorial use only
 *       deposit_item_id: 3366293
 *       views: 91
 *       downloads: 3
 *       level: "beginner"
 *       similar: [
 *               1034007,
 *               1235904,
 *               ....
 *       ],
 *       series : [
 *               1803161,
 *               ....
 *       ]
 *       same_model : [
 *               ....
 *       ]
 *     },
 *     ....
 *     ],
 *     count:13009933,         // The total number of images that meet the query parameters
 *     hash: 1z4ep6,           // search hash of current search phrase and parameters for substitute to depositphotos.com/search.html?
 *     type: "success"         // The query result type
 * }
   */
  search(query, done) {
    return this.api.apiCall('search', {
      'dp_domain': 'depositphotos.com',
      'dp_search_query': query,
      'dp_search_limit': 3,       // TODO Implement
      'dp_search_offset': 0,      // TODO Implement
    }).then(res => res.json())
      .then((res) => {
      if ('success' !== res.type) {
        done(res.error, null);
      }

      done(null, res);
    }).catch((err) => {
      done(err, null);
      throw err;
    });
  }
}

exports.Image = Image;